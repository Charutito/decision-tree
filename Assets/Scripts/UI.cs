﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI : MonoBehaviour {


    public Text foodCountText;
    public Text woodCountText;
    public Text hungryCountText;

	// Use this for initialization
	void Start () {
	
	}
	
	void Update () 
    {
        hungryCountText.text = "Hambre: " + EnviromentData.Instance.hungry.ToString();
        woodCountText.text = "Madera: " + EnviromentData.Instance.wood.ToString();
        foodCountText.text = "Comida: " + EnviromentData.Instance.food.ToString();
	}
}
