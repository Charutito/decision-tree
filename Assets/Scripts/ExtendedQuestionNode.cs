﻿using UnityEngine;
using System.Collections;

public class ExtendedQuestionNode : QuestionNode
{
    public Node opcionA;
    public Node opcionB;
    
    private static ExtendedQuestionNode _instance;
    public static ExtendedQuestionNode Instance { get { return _instance; } }
    
    void Awake()
    {
        _instance = this;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position, trueNode.transform.position);
        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(transform.position, falseNode.transform.position);

        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position, opcionA.transform.position);
        Gizmos.DrawLine(transform.position, opcionB.transform.position);     
    }
}
