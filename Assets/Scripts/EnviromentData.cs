﻿using UnityEngine;
using System.Collections;

public class EnviromentData : MonoBehaviour
{

    public bool isNight = false;
    public bool itsRaining = false;


    public int hungry = 0;
    public float food = 0;
    public float wood = 0;

    public bool hasHungry = false;
    public bool hasFood = false;
    public bool hasWood = false;


	//TODO:
	//Esta clase va a contener toda la información necesaria 
	//para que el aldeano pueda decidir que hacer (acceden a esta info haciendo algo como
	//EnviromentData.Instance.foodQty).
	//Por ejemplo acá pueden poner:
	//1. Cuanta madera tiene (la necesita para construir)
	//2. Cuanta comida tiene (la necesita para vivir)
	//3. Estado del clima (si llueve no debería salir de su casa porque se enferma)
	//4. Momento del día (de noche es peligroso salir)	

	//La siguiente region les va a servir para
	//acceder al aldeano desde sus nodos 'Accion' (deberán heredar de los nodos 'Accion'		
	//y en el método Execute() hacer algo como 'EnviromentData.Instance.citizen.DoSomething()')
#region DONT TOUCH THIS
	public Citizen citizen;
	private static EnviromentData _instance;
	public static EnviromentData Instance {	get	{	return _instance;	}	}
	void Awake()
	{
		_instance = this;
	}
#endregion

    void Update()
    {
        //Tengo Comida?
        if (food < 30)
            hasFood = false;
        else
            hasFood = true;

        //Tengo Hambre?
        if (hungry > 50)
            hasHungry = true;
        else
            hasHungry = false;

        //Tengo madera?
        if (wood > 50)
            hasWood = true;
        else
            hasWood = false; 

    }



}
