﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class QuestionNode : Node
{

    public Questions question;
    public int value;
    public Node trueNode;
    public Node falseNode;


    private Dictionary<Node, int> _actions;
    private float _totalWeight;

    void Start()
    {
        _actions = new Dictionary<Node, int>();

        _actions.Add(trueNode, 2);
        _actions.Add(ExtendedQuestionNode.Instance.opcionA, 3);
        _actions.Add(ExtendedQuestionNode.Instance.opcionB, 5);

        _totalWeight = 10;
    }

    public enum Questions
    {
        IsNight,
        ItsRaining,
        HasFood,
        HasWood,
        HasHungy
    }

    public override void Execute(Citizen reference)
    {
        switch (question)
        {
            case Questions.IsNight:
                if (EnviromentData.Instance.isNight)
                    trueNode.Execute(reference);
                else
                    falseNode.Execute(reference);
                break;
            case Questions.ItsRaining:
                if (EnviromentData.Instance.itsRaining)
                {
                    if (!EnviromentData.Instance.isNight && EnviromentData.Instance.food > 30 && EnviromentData.Instance.hungry > 10)
                    {
                        float rValue = UnityEngine.Random.Range(0, _totalWeight);
                        foreach (KeyValuePair<Node, int> action in _actions)
                        {
                            rValue -= action.Value;
                            if (rValue < 0)
                            {
                                action.Key.Execute(reference);
                                //Debug.Log(action.Key);
                                break;
                            }
                        }
                        //trueNode.Execute(reference);
                    }
                    else
                    trueNode.Execute(reference);
                }
                else
                    falseNode.Execute(reference);
                break;
            case Questions.HasFood:
                if (EnviromentData.Instance.food > 30)
                    trueNode.Execute(reference);
                else
                    falseNode.Execute(reference);
                break;
            case Questions.HasHungy:
                if (EnviromentData.Instance.hungry > 50)
                    trueNode.Execute(reference);
                else
                    falseNode.Execute(reference);
                break;
            case Questions.HasWood:
                if (EnviromentData.Instance.wood > 50)
                    trueNode.Execute(reference);
                else
                    falseNode.Execute(reference);
                break;
        }

    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position, trueNode.transform.position);
        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(transform.position, falseNode.transform.position);
    }

}
