﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DayAndNightCycle : MonoBehaviour {

    void ChangeCycle()
    {
        EnviromentData.Instance.isNight = !EnviromentData.Instance.isNight;
    }
}
