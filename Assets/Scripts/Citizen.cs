﻿using UnityEngine;
using System.Collections;

public class Citizen : MonoBehaviour 
{
    public Node decisionTreeRoot;

    //          TIMERS          //
    private float timeLeftFarm = 2;
    private float timeLeftEat = 2;
    private float timeLeftSleep = 3;
    private float timeLeftCut = 2;
    private float timeLeftBuild = 5;
    private float timeToUpdate = 3;

    public ParticleSystem eatPos;
    public ParticleSystem musicPos;


#region DONT TOUCH THIS
	public ParticleSystem sleepPos;
	public ParticleSystem getWoodPos;
	public ParticleSystem farmingPos;
	public ParticleSystem buildPos;

	public void GetFood()
	{
		Debug.Log("Decision: Get Food!");
		DeactivateAllParticles();		
		SetPosAndPlayParticle(farmingPos);
	}

	public void GetWood()
	{
		Debug.Log("Decision: Get Wood!");
		DeactivateAllParticles();		
		SetPosAndPlayParticle(getWoodPos);
	}

	public void BuildHouses()
	{
		Debug.Log("Decision: Build!");
		DeactivateAllParticles();		
		SetPosAndPlayParticle(buildPos);
	}

	public void GoToSleep()
	{
		Debug.Log("Decision: Go to Sleep!");
		DeactivateAllParticles();		
		SetPosAndPlayParticle(sleepPos);
	}

	private void DeactivateAllParticles()
	{
		sleepPos.Stop();
		getWoodPos.Stop();
		farmingPos.Stop();
		buildPos.Stop();
        eatPos.Stop();
        musicPos.Stop();
	}

	private void SetPosAndPlayParticle(ParticleSystem target)
	{
		transform.position = target.transform.position;
        target.Play();
	}
#endregion

    public void GoToEat()
    {
        Debug.Log("Decision: Go to Eat!");
        DeactivateAllParticles();
        SetPosAndPlayParticle(eatPos);
    }

    public void GoToListenMusic()
    {
        Debug.Log("Decision: Go to Listen Music!");
        DeactivateAllParticles();
        SetPosAndPlayParticle(musicPos);
    }

    void Update()
    {
        timeToUpdate -= Time.deltaTime;
        if (timeToUpdate < 0)
        {
            decisionTreeRoot.Execute(this);
            timeToUpdate = 3;
        }
        
        Farm();
        Eat();
        Sleep();
        Cut();
        Build();
    }

    void Sleep()
    {
        if (transform.position == sleepPos.transform.position)
        {
            timeLeftSleep -= Time.deltaTime;
            if (timeLeftSleep < 0)
            {
                EnviromentData.Instance.hungry += 1;
                timeLeftSleep = 3;
            }
        }
    }
    void Farm()
    {
        if (transform.position == farmingPos.transform.position)
        {
            timeLeftFarm -= Time.deltaTime;
            if (timeLeftFarm < 0)
            {
                EnviromentData.Instance.food += 20;
                EnviromentData.Instance.hungry += 5;
                timeLeftFarm = 2;
            }
        }
    }

    void Eat()
    {
        if (transform.position == eatPos.transform.position)
        {
            timeLeftEat -= Time.deltaTime;
            if (timeLeftEat < 0)
            {
                EnviromentData.Instance.food -= 10;
                EnviromentData.Instance.hungry -= 10;
                timeLeftEat = 2;
            }
        }
    }

    void Cut()
    {
        if (transform.position == getWoodPos.transform.position)
        {
            timeLeftCut -= Time.deltaTime;
            if (timeLeftCut < 0)
            {
                EnviromentData.Instance.wood += 10;
                EnviromentData.Instance.hungry += 5;
                timeLeftCut = 2;
            }
        }
    }

    void Build()
    {
        if (transform.position == buildPos.transform.position)
        {
            timeLeftBuild -= Time.deltaTime;
            if (timeLeftBuild < 0)
            {
                EnviromentData.Instance.hungry += 10;
                EnviromentData.Instance.wood -= 20;
                timeLeftBuild = 5;
            }
        }
    }

}
