﻿using UnityEngine;
using System.Collections;

public class BuildAction : ActionNode {

    public override void Execute(Citizen reference)
    {
        EnviromentData.Instance.citizen.BuildHouses();
    }
}
