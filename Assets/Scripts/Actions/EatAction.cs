﻿using UnityEngine;
using System.Collections;

public class EatAction : ActionNode {

    public override void Execute(Citizen reference)
    {
        EnviromentData.Instance.citizen.GoToEat();
    }
}
