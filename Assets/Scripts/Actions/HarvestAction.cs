﻿using UnityEngine;
using System.Collections;

public class HarvestAction : ActionNode {

    public override void Execute(Citizen reference)
    {
        EnviromentData.Instance.citizen.GetFood();
    }
}
