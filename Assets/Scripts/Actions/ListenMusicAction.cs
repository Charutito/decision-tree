﻿using UnityEngine;
using System.Collections;

public class ListenMusicAction : ActionNode
{
    public override void Execute(Citizen reference)
    {
        EnviromentData.Instance.citizen.GoToListenMusic();
    }
}
